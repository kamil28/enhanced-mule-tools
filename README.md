**IMPORTANT NOTE**

This project's maintenance has been taken over by Aeontronix (please note maven group id has now changed, see docs).

Documentation: https://aeontronix.gitlab.io/oss/enhanced-mule-tools/

Repository URL: https://gitlab.com/aeontronix/oss/enhanced-mule-tools
