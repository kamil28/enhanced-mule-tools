/*
 * Copyright (c) Aeontronix 2019
 */

package com.aeontronix.enhancedmule.tools.alert;

public enum AlertSeverity {
    CRITICAL, WARNING, INFO
}
