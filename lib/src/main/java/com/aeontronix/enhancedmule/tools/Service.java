/*
 * Copyright (c) Aeontronix 2019
 */

package com.aeontronix.enhancedmule.tools;

public interface Service {
    void setClient(AnypointClient client);
}
